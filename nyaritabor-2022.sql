-- szerda vacsorával kezdődik 2022-ben a tábor,
-- így bekerült a szerda vacsora és a csütörtök reggeli.
-- 2022. febr. 16.

ALTER TABLE `resztvevo`
ADD `szerdaVacs` ENUM('egesz','fel','nem') NOT NULL AFTER `altabor`,
ADD `csutortokReg` ENUM('egesz','fel','nem') NOT NULL AFTER `szerdaVacs`;
