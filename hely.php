<?php

require("parameterek.php");

$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);

// Oh no! A connect_errno exists so the connection attempt failed!
if ($mysqli->connect_errno) {

    echo "Sorry, this website is experiencing problems.";

    // Something you should not do on a public site, but this example will show you
    // anyways, is print out MySQL error related information -- you might log this
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    
    // You might want to show them something nice, but we will simply exit
    exit;
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
    exit();
}

// Get the number of rows from `resztvevok` table
$mysql_query = "SELECT COUNT(*) FROM `resztvevo`;";

if (!$result = $mysqli->query($mysql_query)) {
    // Oh no! The query failed. 
    echo "A jelentkezések megszámolása nem sikerült!";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $mysql_query . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

$hely = $max_hely - $result->fetch_row()[0];

if ($hely <= 0) {
    echo "<b>Már nincs több jelentkezőnek hely a táborban! A további jelentkezéseket regisztáljuk, várólistára kerülnek, és ha valaki lemond, akkor értesítjük őket.</b>";
} else if ($hely <= 10) {
    echo "<b>Már csak ".$hely."-en tudnak jelentkezni a táborba! A további jelentkezéseket regisztáljuk, várólistára kerülnek, és ha valaki lemond, akkor értesítjük őket.</b>";
}


// lekérem az adatokat szoba típusonként, majd javasciptben válozóként visszaadom
$mysql_query = "SELECT `szallTipus`, COUNT(*) FROM `resztvevo` GROUP BY `szallTipus`;";

if (!$result = $mysqli->query($mysql_query)) {
    // Oh no! The query failed. 
    echo "A szabad szobák megszámolása nem sikerült!";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $mysql_query . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

$script_string = "<script>";
$script_string .= "var maradHely = {"; // array of remaining places

// enum('egyagyas','egyagyas-1potagy','ketagyas','ketagyas-1potagy','ketagyas-2potagy','hatagyas','tizennegyagyas','sator','matrac','nem') -> "egyagyas", "ketagyas", "hatagyas", "tizennegyagyas"
// map these values
$szoba_map = [
    "egyagyas" => "egyagyas",
    "egyagyas-1potagy" => "egyagyas",
    "ketagyas" => "ketagyas",
    "ketagyas-1potagy" => "ketagyas-1potagy",
    "ketagyas-2potagy" => "ketagyas-2potagy",
    "hatagyas" => "hatagyas",
    "tizennegyagyas" => "tizennegyagyas",
    "sator" => "semmi",
    "matrac" => "semmi",
    "nem" => "semmi"
];

// map these values
$maradek_hely = [
    "egyagyas" => $helyek_szama["egyagyas"],
    "ketagyas" => $helyek_szama["ketagyas"],
    "ketagyas-1potagy" => $helyek_szama["ketagyas-1potagy"],
    "ketagyas-2potagy" => $helyek_szama["ketagyas-2potagy"],
    "hatagyas" => $helyek_szama["hatagyas"],
    "tizennegyagyas" => $helyek_szama["tizennegyagyas"]
];

while ($row = $result->fetch_row()) {
    // true if $row[1] is less than $helyek_szama[$row[0]]
    //$script_string .= $row[0]." = ".($row[1] < $helyek_szama[$row[0]] ? "true" : "false").",";
    if ($szoba_map[$row[0]] == "semmi") continue; // skip "semmi" (sator, matrac, nem)
    $maradek_hely[$szoba_map[$row[0]]] -= $row[1];
}

$script_string .= "egyagyas: " . ($maradek_hely["egyagyas"] > 0 ? "true" : "false").",";
$script_string .= "ketagyas: " . ($maradek_hely["ketagyas"] > 0 ? "true" : "false").",";
$script_string .= "\"ketagyas-1potagy\": " . ($maradek_hely["ketagyas-1potagy"] > 0 ? "true" : "false").",";
$script_string .= "\"ketagyas-2potagy\": " . ($maradek_hely["ketagyas-2potagy"] > 0 ? "true" : "false").",";
$script_string .= "hatagyas: " . ($maradek_hely["hatagyas"] > 0 ? "true" : "false").",";
$script_string .= "tizennegyagyas: " . ($maradek_hely["tizennegyagyas"] > 0 ? "true" : "false");

$script_string .= "};";
$script_string .= "</script>";

echo $script_string;

?>