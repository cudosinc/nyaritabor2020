-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Dec 14, 2019 at 01:41 PM
-- Server version: 5.7.28
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nyaritabor`
--

-- --------------------------------------------------------

--
-- Table structure for table `jelentkezes`
--

DROP TABLE IF EXISTS `jelentkezes`;
CREATE TABLE IF NOT EXISTS `jelentkezes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tamogatasAdas` int(11) DEFAULT NULL,
  `satorhelyekSzama` int(11) DEFAULT NULL,
  `vegosszeg` int(11) NOT NULL,
  `halotarsak` varchar(200) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `busz` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `tamogatasKeres` int(11) DEFAULT NULL,
  `feladat` varchar(200) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `uzenet` text COLLATE utf8_hungarian_ci,
  `kapcsNev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kapcsEmail` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `kapcsMobil` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `jelentkezesDatum` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modDatum` datetime DEFAULT NULL,
  `URL` varchar(32) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resztvevo`
--

DROP TABLE IF EXISTS `resztvevo`;
CREATE TABLE IF NOT EXISTS `resztvevo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jelentkezes_id` int(11) NOT NULL,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `szulido` date NOT NULL,
  `szallTipus` enum('egyagyas','ketagyas','ketagyas-potagy','ketagyas-kisbaba','hatagyas','tizennegyagyas','sator','matrac','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `ejSzam` enum('2','3') COLLATE utf8_hungarian_ci NOT NULL,
  `altabor` enum('igen','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `csutortokEbed` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `csutortokVacs` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `pentekReg` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `pentekEbed` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `pentekVacs` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `szombatReg` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `szombatEbed` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `szombatVacs` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `vasarnapReg` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `vasarnapEbed` enum('egesz','fel','nem') COLLATE utf8_hungarian_ci NOT NULL,
  `allergia` text COLLATE utf8_hungarian_ci,
  PRIMARY KEY (`id`),
  INDEX `jelentkezes_id_index` (`jelentkezes_id`),
  CONSTRAINT fk_jelentkezes_id FOREIGN KEY jelentkezes_id_index (jelentkezes_id)
    REFERENCES jelentkezes (id)
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
