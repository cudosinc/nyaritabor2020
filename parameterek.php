<?php

// segítség a MySQL felhasználó és jelszó létrehozásához:
// CREATE user 'nyaritabor'@'localhost' identified by 'nyaritabor'
// GRANT ALL PRIVILEGES ON nyaritabor.* TO 'nyaritabor'@'localhost';
// FLUSH PRIVILEGES;

$db_server = "127.0.0.1";
$db_user = "nyaritabor";
$db_pass = "nyaritabor";
$db_name = "nyaritabor";

$email_kuldes = true; // Küldjön vagy ne küldjön levelet - tesztelés során zavaró, ha mindig küld.

$email_from = "nyaritabor@hvj.hu";
$email_szervezok = "horvath.varga.janos+nyaritabor@gmail.com"; // ők BCC-ben kapnak másolatot a levélből
$email_henrik = "henrik.rosa@gmail.com"; // szintén BCC-ben kap levelet
$email_fejlesztok = "horvath.varga.janos+nyaritabor@gmail.com"; // ők BCC-ben kaphatnak másolatot a levélből
//$email_subject_prefix = "[nyaritabor-regisztracio] ";
$email_subject_prefix = ""; // talán kevésbé zavaró az átlagembernek, ha nincs...

$email_smtp_host = "smtp.websupport.sk";
$email_smtp_port = 465;
$email_smtp_auth = true;
$email_smtp_auth_type = 'ssl';
$email_smtp_user = "nyaritabor@hvj.hu";
$email_smtp_password = "password";

// ennyien lehet jelenkezni a táborba
$max_hely = 150;

$helyek_szama = [
    "egyagyas" => 1,
    "ketagyas" => 12*2,
    "ketagyas-1potagy" => 10*3,
    "ketagyas-2potagy" => 1*4,
    "hatagyas" => 7*6,
    "tizennegyagyas" => 3*14
];

?>