<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';

require("parameterek.php");

// a regisztrációs űrlap adatait és a modellek app.js által számolt adatait is elküldi AJAX hívásban a kliens.
// feladatok:
// - permalink url-t generálni a jelentkezéshez
// - az adatbázisba eltárolni a jelentkező adatait (később, ha a módosításra is képes lesz a rendszer, akkor frissíteni a korábbi regisztrációt)
// - visszaigazoló weboldalt küldeni a kliens felé
// - visszaigazoló emailt küldeni a jelentkező szervezőnek
// - értesítő emailt küldeni a szervezőknek

//var_dump($_POST);

$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);

// Oh no! A connect_errno exists so the connection attempt failed!
if ($mysqli->connect_errno) {

    echo "Sorry, this website is experiencing problems.";

    // Something you should not do on a public site, but this example will show you
    // anyways, is print out MySQL error related information -- you might log this
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    
    // You might want to show them something nice, but we will simply exit
    exit;
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
    exit();
}

//---------------------------------------------------
// 1. a jelentkezést rögzítjük az adatbázisba

// TODO: később, ha lesz módosításra lehetőség, akkor először törölni kell a már létező jelentkezést
// a hozzá tartozó résztvevőkkel (cascade segítségével, hogy egyszerű legyen)
// és csak utána insertálni a módosított, új jelentkezést.

// az egyes mezők minimális ellenőrzése (alapvetően javascript oldalon ellenőrizünk, nem lesz hacker védett a szerver)

if (empty($_POST['tamogatasAdas'])) $_POST['tamogatasAdas']='NULL';
if (empty($_POST['tamogatasKeres'])) $_POST['tamogatasKeres']='NULL';

if (empty($_POST['URL'])) $_POST['URL'] = md5(uniqid());

$sql_jelentkezes = "INSERT INTO jelentkezes 
(
    kapcsNev,
    kapcsEmail,
    kapcsMobil,
    jelentkezesDatum,
    tamogatasAdas,
    tamogatasKeres,
    halotarsak,
    vegosszeg,
    busz,
    oszkar,
    feladat,
    uzenet,
    adatkezeles_hozzajarul,
    URL
)
VALUES
(
    \"".$_POST['kapcsNev']."\",
    \"".$_POST['kapcsEmail']."\",
    \"".$_POST['kapcsMobil']."\",
    NOW(),
    ".$_POST['tamogatasAdas'].",
    ".$_POST['tamogatasKeres'].",
    \"".$_POST['halotarsak']."\",
    ".$_POST['vegosszeg'].",
    \"".$_POST['busz']."\",
    \"".$_POST['oszkar']."\",
    \"".$_POST['feladat']."\",
    \"".$_POST['uzenet']."\",
    \"".$_POST['adatkezeles_hozzajarul']."\",
    \"".$_POST['URL']."\"
);";

if (!$result = $mysqli->query($sql_jelentkezes)) {
    // Oh no! The query failed. 
    echo "A jelentkezés eltárolása nem sikerült!";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql_jelentkezes . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}
$jelentkezes_id = $mysqli->insert_id;

//---------------------------------------------------
// 2. a résztvevőket rögzítjük az adatbázisba

foreach ($_POST['htmlIds'] as $htmlId) {
    
    // Jancsi nap van - Bogi írása

    /*$sql_resztvevo = "INSERT INTO resztvevo 
    (
        jelentkezes_id,
        nev,
        szulhely,
        szulido,
        lakcim,
        szallTipus,
        ejSzam,
        altabor,
        szerdaVacs,
        csutortokReg,
        csutortokEbed,
        csutortokVacs,
        pentekReg,
        pentekEbed,
        pentekVacs,
        szombatReg,
        szombatEbed,
        szombatVacs,
        vasarnapReg,
        vasarnapEbed,
        allergia
    )
    VALUES
    (
        ".$jelentkezes_id.",
        \"".$_POST['nev'.$htmlId]."\",
        \"".$_POST['szulhely'.$htmlId]."\",
        \"".$_POST['szulido'.$htmlId]."\",
        \"".$_POST['lakcim'.$htmlId]."\",
        \"".$_POST['szallTipus'.$htmlId]."\",
        \"".$_POST['ejSzam'.$htmlId]."\",
        \"".$_POST['altabor'.$htmlId]."\",
        \"".$_POST['szerdaVacs'.$htmlId]."\",
        \"".$_POST['csutortokReg'.$htmlId]."\",
        \"".$_POST['csutortokEbed'.$htmlId]."\",
        \"".$_POST['csutortokVacs'.$htmlId]."\",
        \"".$_POST['pentekReg'.$htmlId]."\",
        \"".$_POST['pentekEbed'.$htmlId]."\",
        \"".$_POST['pentekVacs'.$htmlId]."\",
        \"".$_POST['szombatReg'.$htmlId]."\",
        \"".$_POST['szombatEbed'.$htmlId]."\",
        \"".$_POST['szombatVacs'.$htmlId]."\",
        \"".$_POST['vasarnapReg'.$htmlId]."\",
        \"".$_POST['vasarnapEbed'.$htmlId]."\",
        \"".$_POST['allergia'.$htmlId]."\"
    );";*/


    // 2023-as verzió
    $sql_resztvevo = "INSERT INTO resztvevo 
    (
        jelentkezes_id,
        nev,
        szulhely,
        szulido,
        lakcim,
        szallTipus,
        ejSzam,
        etkezes,
        allergia
    )
    VALUES
    (
        ".$jelentkezes_id.",
        \"".$_POST['nev'.$htmlId]."\",
        \"".$_POST['szulhely'.$htmlId]."\",
        \"".$_POST['szulido'.$htmlId]."\",
        \"".$_POST['lakcim'.$htmlId]."\",
        \"".$_POST['szallTipus'.$htmlId]."\",
        \"".$_POST['ejSzam'.$htmlId]."\",
        \"".$_POST['etkezes'.$htmlId]."\",
        \"".$_POST['allergia'.$htmlId]."\"
    );";
    
    
    if (!$result = $mysqli->query($sql_resztvevo)) {
        // Oh no! The query failed. 
        echo "A résztvevő eltárolása nem sikerült!";

        // Again, do not do this on a public site, but we'll show you how
        // to get the error information
        echo "Error: Our query failed to execute and here is why: \n";
        echo "Query: " . $sql_resztvevo . "\n";
        echo "Errno: " . $mysqli->errno . "\n";
        echo "Error: " . $mysqli->error . "\n";
        exit;
    }

}

$mysqli->close();

//--------------------------------------------------------------------------------------
// visszaigazoló oldal generálása (ebből lesz a webre válasz és a html email törzse is)

/*function adag($a) {

    switch ($a) {
        case "egesz":
            return("egész adag");
        case "fel":
            return("fél adag");
        case "nem":
            return("nem kérek");
    }
        
}*/

$valasz_torzs = "<h2>Köszönjük a jelentkezést a 2022-es egyházközségi nyári táborra!</h2>
<p>A jelentkezést az alábbi adatokkal rögzítettük:</p>
<b>Kapcsolattartó:</b><br/>
&nbsp;&nbsp;neve: ".$_POST['kapcsNev']."<br/>
&nbsp;&nbsp;email címe: ".$_POST['kapcsEmail']."<br/>
&nbsp;&nbsp;telefonszáma: ".$_POST['kapcsMobil']."<br/>

<p><b>Résztvevők:</b><br/>";

$i=1;
foreach ($_POST['htmlIds'] as $htmlId) {

    $valasz_torzs .= "<p><h3>$i. ".$_POST['nev'.$htmlId]."</h3>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;Születési hely és idő: ".$_POST['szulhely'.$htmlId].", ".$_POST['szulido'.$htmlId]."<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;Lakcím: ".$_POST['lakcim'.$htmlId]."<br/>\r\n";

    $valasz_torzs .= "&nbsp;&nbsp;Éjszakák száma: ".$_POST['ejSzam'.$htmlId]."<br/>\r\n&nbsp;&nbsp;";

    switch ($_POST['szallTipus'.$htmlId]) {
        case "semmi":
            $valasz_torzs .="Nem kér szállást<br/>\r\n";
        break;
        case "egyagyas":
            $valasz_torzs .="Egy ágyas szoba<br/>\r\n";
        break;
        case "ketagyas":
            $valasz_torzs .="Két ágyas szoba<br/>\r\n";
        break;
        case "ketagyas-potagy":
            $valasz_torzs .="Két ágyas szoba pótágya<br/>\r\n";
        break;
        case "ketagyas-kisbaba":
            $valasz_torzs .="Két ágyas szobában 0-2 éves kisbaba (ingyenes)<br/>\r\n";
        break;
        case "hatagyas":
            $valasz_torzs .="Hat ágyas szoba<br/>\r\n";
        break;
        case "matrac":
            $valasz_torzs .="Hat ágyas hálóban +1 saját matracon (ingyenes)<br/>\r\n";
        break;
        case "tizennegyagyas":
            $valasz_torzs .="14 ágyas szoba<br/>\r\n";
        break;
        case "sator":
            $valasz_torzs .="Sátor<br/>\r\n";
        break;
    }

    if ($_POST['etkezes'.$htmlId]=="igen") $valasz_torzs .= "<br>\r\nÉtkezést kért<br/>\r\n";
    else $valasz_torzs .= "<br>\r\nÉtkezést nem kért<br/>\r\n";

    //if ($_POST['altabor'.$htmlId]=="igen") $valasz_torzs .= "Gyermek altáborban részt vesz.<br/>\r\n";
    //$valasz_torzs .= "<br/>\r\n";

    /*$valasz_torzs .= "<b>Igényelt ellátás</b><br/>\r\n";
    $valasz_torzs .= "<br/>\r\n";

    $valasz_torzs .= "Szerda:<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;vacsora: ".adag($_POST['szerdaVacs'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "<br/>\r\n";

    $valasz_torzs .= "Csütörtök:<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;reggeli: ".adag($_POST['csutortokReg'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;ebéd: ".adag($_POST['csutortokEbed'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;vacsora: ".adag($_POST['csutortokVacs'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "<br/>\r\n";

    $valasz_torzs .= "Péntek:<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;reggeli: ".adag($_POST['pentekReg'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;ebéd: ".adag($_POST['pentekEbed'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;vacsora: ".adag($_POST['pentekVacs'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "<br/>\r\n";

    $valasz_torzs .= "Szombat:<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;reggeli: ".adag($_POST['szombatReg'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;ebéd: ".adag($_POST['szombatEbed'.$htmlId])."<br />\r\n";
    //$valasz_torzs .= "&nbsp;&nbsp;vacsora: ".adag($_POST['szombatVacs'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "<br/>\r\n";*/
/*
    $valasz_torzs .= "Vasárnap:<br/>\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;reggeli: ".adag($_POST['vasarnapReg'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "&nbsp;&nbsp;ebéd: ".adag($_POST['vasarnapEbed'.$htmlId])."<br />\r\n";
    $valasz_torzs .= "<br/>\r\n";
*/
    $valasz_torzs .= "Étel allergia: ";
    if (empty($_POST['allergia'.$htmlId])) $valasz_torzs .= "nincs megadva";
    else $valasz_torzs .= $_POST['allergia'.$htmlId];
    $valasz_torzs .= "<br/>\r\n";
    $valasz_torzs .= "</p>\r\n"; // aktuális résztvevő lezárása

    $i++;
}
$valasz_torzs .= "</p>\r\n"; // résztvevők listájának lezárása

if (!empty($_POST['halotarsak'])) $valasz_torzs .= "Ha lehetséges, velük szeretnél közös hálót igényelni: ".$_POST['halotarsak']."<br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

if (!($_POST['tamogatasAdas']=='NULL')) $valasz_torzs .= "<b>Köszönjük, hogy az alábbi összegű támogatást ajánlottad fel: ".$_POST['tamogatasAdas']." Ft</b><br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

$valasz_torzs .= "<b>A nyári tábor összköltsége: ".$_POST['vegosszeg']." Ft</b><br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

if (!($_POST['tamogatasKeres']=='NULL')) $valasz_torzs .= "Az alábbi összegű támogatást kérted: ".$_POST['tamogatasKeres']." Ft<br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

if (!empty($_POST['busz'])) $valasz_torzs .= "Meg tudod-e oldani az oda-vissza utat: ".$_POST['busz']."<br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

if (!empty($_POST['oszkar'])) $valasz_torzs .= "Tudsz-e saját autóban elhozni valakit a táborba? Ha igen, hány személyt? ".$_POST['oszkar']."<br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

$valasz_torzs .= "Részvétel a tábori feladatokban: ".$_POST['feladat']."<br/>\r\n";
$valasz_torzs .= "<br/>\r\n";

$valasz_torzs .= "Egyéb üzenet, megjegyzés a szervezőknek:<br/>\r\n<pre>".$_POST['uzenet']."</pre><br/>\r\n";
$valasz_torzs .= "<br/>\r\n";


$valasz_torzs .= "<p>Ha a jelentkezést módosítani szeretnéd vagy a fenti adatok közül<br/>bármi nem stimmel, illetve további információért kérlek keresd Barnáné Varga Anitát (varganita@hotmail.com) vagy Rósa Henriket (henrik.rosa@gmail.com)!</p>
<p>
A fizetés kizárólag utalás formájában lehetséges!<br/>
A befizetés részleteiről külön értesítést fogunk küldeni. (Még nem lehet utalni, nehogy fizetés után változás történjen.)<br/>
</p>
<h2>Köszönjük szépen!</h2>
A szervezők nevében: Barnáné Varga Anita<br/>
";

echo $valasz_torzs;

// a választ elmentem a szerverre is, hogy később problémák nyomozása esetén lássuk, mit kapott vissza a jelentkező:
if ($fp = fopen("jelentkezesek/".$_POST['URL'].".html","w")) {
    fwrite($fp,$valasz_torzs);
    fclose($fp);
}

#------------------------------------------------------------------------------------------------------------------

// levélküldés

if ($email_kuldes) {
    $mail = new PHPMailer;

    $mail->isSMTP();                            // Set mailer to use SMTP
    $mail->Host = $email_smtp_host;             // Specify main and backup SMTP servers
    $mail->SMTPAuth = $email_smtp_auth;                     // Enable SMTP authentication
    $mail->SMTPSecure = $email_smtp_auth_type;                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $email_smtp_port;                          // TCP port to connect to
    $mail->Username = $email_smtp_user;          // SMTP username
    $mail->Password = $email_smtp_password; // SMTP password
    $mail->CharSet = 'UTF-8';

    // debugging célból, 
    $mail->SMTPDebug = 0;

    $mail->setFrom($email_from, 'Egyházközségi tábor regisztráció');
    $mail->addReplyTo($email_szervezok, 'Barnáné Varga Anita');
    $mail->addAddress($_POST['kapcsEmail']);   // TO mező!!!
    $mail->addBCC($email_szervezok);
    $mail->addBCC($email_henrik);
    $mail->addBCC($email_fejlesztok);

    $mail->isHTML(true);  // Set email format to HTML

    $mail->Subject = $email_subject_prefix."Sikeres egyházközségi tábor regisztráció - ".$_POST['kapcsNev'];
    $mail->Body    = "<h1>Kedves " . $_POST['kapcsNev'] . "!</h1>" . $valasz_torzs;

    if(!$mail->send()) {
        echo 'Az értesítő email elküldése ugyan nem sikerült, de a regisztráció megtörtént!';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        //echo 'Message has been sent';
    }
}
?>