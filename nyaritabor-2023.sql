-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Dec 14, 2019 at 01:41 PM
-- Server version: 5.7.28
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `resztvevo`;
DROP TABLE IF EXISTS `jelentkezes`;

--
-- Database: `nyaritabor`
--

-- --------------------------------------------------------

--
-- Table structure for table `jelentkezes`
--

CREATE TABLE IF NOT EXISTS `jelentkezes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tamogatasAdas` int DEFAULT NULL,
  `vegosszeg` int NOT NULL,
  `halotarsak` varchar(100) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
  `busz` varchar(50) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
  `oszkar` varchar(50) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
  `tamogatasKeres` int DEFAULT NULL,
  `feladat` varchar(150) COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
  `uzenet` text COLLATE utf8mb4_hungarian_ci,
  `kapcsNev` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `kapcsEmail` varchar(200) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `kapcsMobil` varchar(30) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `adatkezeles_hozzajarul` enum('igen','nem') COLLATE utf8mb4_hungarian_ci NOT NULL DEFAULT 'nem',
  `jelentkezesDatum` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modDatum` datetime DEFAULT NULL,
  `URL` varchar(32) COLLATE utf8mb4_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resztvevo`
--

CREATE TABLE IF NOT EXISTS `resztvevo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `jelentkezes_id` int NOT NULL,
  `nev` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `szulhely` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `szallTipus` enum('egyagyas','egyagyas-1potagy','ketagyas','ketagyas-1potagy','ketagyas-2potagy','hatagyas','tizennegyagyas','sator','matrac','nem') COLLATE utf8mb4_hungarian_ci NOT NULL,
  `szulido` date NOT NULL,
  `lakcim` varchar(50) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `ejSzam` enum('0', '2cs', '2p','3') COLLATE utf8mb4_hungarian_ci NOT NULL,
  `etkezes` enum('igen','nem') COLLATE utf8mb4_hungarian_ci NOT NULL,
  `allergia` text COLLATE utf8mb4_hungarian_ci,
  PRIMARY KEY (`id`),
  INDEX `jelentkezes_id_index` (`jelentkezes_id`),
  CONSTRAINT fk_jelentkezes_id FOREIGN KEY jelentkezes_id_index (jelentkezes_id)
    REFERENCES jelentkezes (id)
    ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;
COMMIT;
