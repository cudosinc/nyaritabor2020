var verzio = "v0.9.5 (2023.04.16. 13:36)";
var resztvevoIndex = 0;
var resztvevokSzama = 0;
var resztvevoSablon = "";
class Resztvevo {
    constructor(HTMLid) {
        this.id = 0;
        this.HTMLid = HTMLid;
        this.jelentkezes_id = 0;
        this.nev = "";
        this.szulhely = '';
        this.szulido = 0;
        this.lakcim = '';
        this.eletkor = 0;
        this.szallTipus = "";
        this.ejSzam = null; // csak szám
        this.ejSzamNappal = ""; // 0, 2cs, 2p, vagy 3 (melyik két éjszaka)
        this.etkezes = "igen";
        //this.altabor = "nem";
        //this.feladag = "egesz";
        /*this.kaja = {
            szerda: [],
            csutortok: [],
            pentek: [],
            szombat: [],
            vasarnap: ["semmi"] //ezt azért állítom be, mert ha később a from-on újra lesz vasárnap, akkor a kód lentebbi részein már ne kelljen módosítani, csak ezt üresre állítani itt.
        };*/
        /*this.szerdaVacs = "";
        this.csutortokReg = "";
        this.csutortokEbed = "";
        this.csutortokVacs = "";
        this.pentekReg = "";
        this.pentekEbed = "";
        this.pentekVacs = "";
        this.szombatReg = "";
        this.szombatEbed = "";
        this.szombatVacs = "";
        this.vasarnapReg = "";
        this.vasarnapEbed = "";*/
        this.allergia = "";
    }
};
class Jelentkezes {
    constructor() {
        this.id = 0;
        this.csaladLakcime = '';
        this.tamogatasAdas = null;
        this.vegosszeg = 0;
        this.halotarsak = "";
        this.busz = "";
        this.oszkar = "";
        this.tamogatasKeres = null;
        this.feladat = "";
        this.uzenet = "";
        this.kapcsNev = "";
        this.kapcsEmail = "";
        this.kapcsMobil = "";
        this.jelentkezesDatum = 0;
        this.modDatum = 0;
        this.URL = "";
        //this.satorhelyekSzama = 0;
        this.bekuldheto = false; // ha bárhol bármi hiba van (pl.végösszeg nem számítható még), ezt false-re kell tenni és akkor nem küldi be a jelentkezést.
        // "Hozzájárulok ahhoz, hogy a táborban készülő kép-, hang- és videofelvételeket a Plébánia az adatkezelési tájékoztatóban foglaltak szerint kezelje."
        this.adatkezeles_hozzajarul = "nem";
    }
};
var resztvevok = [];
var ezAJelentkezes = new Jelentkezes();

function sablonBetoltes(filename, muveletSzama) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        switch (muveletSzama) {
            case 0:
                if (this.readyState == 4 && this.status == 200) {
                    resztvevoSablon = this.responseText;
                    sorHozzafuzes();
                }
                break;

            case 1:
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("urlaphelye").innerHTML = this.responseText;
                    sablonBetoltes('resztvevo.html', 0);
                }
        }
    }
    xhttp.open("GET", filename);
    xhttp.send();
}

function sorHozzafuzes() {
  var modResztvevo1 = resztvevoSablon.replace(/@/g, resztvevoIndex);
  //alert(modResztvevo1);
  var modResztvevo2 = document.createElement("TR");
  modResztvevo2.innerHTML = modResztvevo1;
  modResztvevo2.id = "resztvevo" + resztvevoIndex;
  //alert(modResztvevo2);
  document.getElementById("adatTabla").appendChild(modResztvevo2);
  resztvevok[resztvevoIndex] = new Resztvevo(resztvevoIndex);

  //beállíttom a lakcímet a családiLakcímre:
  let lakcimmezo = document.getElementById("lakcim" + resztvevoIndex);
  lakcimmezo.value = ezAJelentkezes.csaladLakcime;
  // azonnal elteszem a javascript tömbbe is, mivel a fenti másolás okán a user valószínűleg nem fog onchange event-et generálni
  // a lakcím input field-be, így nem kerülne be ide az adat:
  resztvevok[resztvevoIndex]["lakcim"] = ezAJelentkezes.csaladLakcime;

  // if the proper values in maradHely is false, append " (betelt, várólista)" to the select options
    if (!maradHely["egyagyas"]) {
        // select the option tag with value="egyagyas" and append " (betelt, várólista)" to its text
        // only apply to option in the select with name="szallTipusx" (x is the index of the resztvevo)
        //document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="egyagyas"]').innerHTML += " (betelt, várólista)";
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="egyagyas-1potagy"]').innerHTML += " (betelt, várólista)";
    }
    if (!maradHely["ketagyas"]) {
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="ketagyas"]').innerHTML += " (betelt, várólista)";
    }
    if (!maradHely["ketagyas-1potagy"]) {
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="ketagyas-1potagy"]').innerHTML += " (betelt, várólista)";
    }
    if (!maradHely["ketagyas-2potagy"]) {
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="ketagyas-2potagy"]').innerHTML += " (betelt, várólista)";
    }
    if (!maradHely["hatagyas"]) {
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="hatagyas"]').innerHTML += " (betelt, várólista)";
    }
    if (!maradHely["tizennegyagyas"]) {
        document.querySelector('select[name="szallTipus' + resztvevoIndex + '"] option[value="tizennegyagyas"]').innerHTML += " (betelt, várólista)";
    }
    
  resztvevoIndex++;
  resztvevokSzama++;
}

function eletkorSzamitas(e) {
    szuletett = new Date(e.value);
    tabor = new Date("2023-07-27");
    // a két dátum különbségét ezredmásodpercben kapjuk meg, az évekhez ezt osztani kell:
    // (tabor-szuletett)/1000/86400/365
    evek = (tabor-szuletett)/31536000000;
    //console.log(evek);
    var sorszam = e.name.substr(7);
    resztvevok[sorszam]["eletkor"] = evek;
    //alert(evek<9 || evek>19);
    /*var altaborInput = document.getElementById("altabor" + sorszam);
    if (evek<9 || evek>19) {
        altaborInput.value = "nem";
        altaborInput.disabled = "true";
    } else {
        altaborInput.value = "nem";
        altaborInput.disabled = "";
    }*/
    // fél adag ételt 6 év felett nem lehet kérni:
    /*if (evek>6) {
        document.getElementById("feladag"+sorszam).value="egesz";
    }*/
}


/*const kajaArak = {
    reg: {
        egesz: 800,
        fel: 500
    },
    ebed: {
        egesz: 2000,
        fel: 1000
    },
    vacs: {
        egesz: 1500,
        fel: 800
    },
    semmi: {
        egesz: 0,
        fel: 0
    }
};*/

// 2023 verzió:
const kajaArak = {
    egesz: 5400,
    fel: 2920
};

const szallasArak = {
    zarandokHaz: { // Itt vannak az 1 és 2 ágyas szobák (pótággyal vagy a nélkül). Ahányan vannak a szobában, annyit fizetnek.
        felnott: 5500, //8 év felett
        gyerek: 2750, //3-8 év között, de 3 év alatt ingyenes
    },
    zarandokSzallas: { // vannak a 6 és 14 ágyas szobák.
        felnott: 4000,
        gyerek: 2000
    },
    sator: { // fejenként! (végre!)
        felnott: 1500,
        gyerek: 1500
    },
    ingyen: {
        felnott: 0,
        gyerek: 0
    }
};

const holvan = {
    egyagyas: "zarandokHaz",
    "egyagyas-1potagy": "zarandokHaz",
    ketagyas: "zarandokHaz",
    "ketagyas-1potagy": "zarandokHaz",
    "ketagyas-2potagy": "zarandokHaz",
    hatagyas: "zarandokSzallas",
    tizennegyagyas: "zarandokSzallas",
    //"ketagyas-kisbaba": "ingyen",
    matrac: "ingyen",
    nem: "ingyen",
    sator: "sator"
};

const IFA = 300; // 18-70 év között

function tagolas(osszeg) {
    // 'osszeg' is a number
    // format the number with spaces as thousands separators
    return osszeg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "); // tök menő regex, bár nem én dobtam össze, hanem a Github Copilot :D
}

function koltsegSzamitas() {
    let osszeg = 0, nevek = [], fejenkent = [], kajabol = [], szallasbol = [], ifabol = [], satorbol = 0;
    ezAJelentkezes.bekuldheto = false;

    for (let i = 0, j = 0; i < resztvevok.length; i++, j++) {
        if (resztvevok[i] === undefined) {
            j--;
            continue;
        }

        nevek.push(resztvevok[i].nev);
        fejenkent.push(0);
        kajabol.push(0);
        szallasbol.push(0);
        ifabol.push(0);

        // étkezés árának kiszámítása:
        /*for (let nap in resztvevok[i]["kaja"]) {
            for (let kaja of resztvevok[i]["kaja"][nap]) {
                osszeg += kajaArak[kaja][resztvevok[i]["feladag"]];
                kajabol[j] += kajaArak[kaja][resztvevok[i]["feladag"]];
                fejenkent[j] += kajaArak[kaja][resztvevok[i]["feladag"]];
            }
        }*/
        if (resztvevok[i]["etkezes"] == "igen") {
            // 6 év alatt féladag:
            let feladag = (resztvevok[i].eletkor <= 6) ? "fel" : "egesz";
            console.log(feladag);
            osszeg += kajaArak[feladag] * resztvevok[i].ejSzam;
            kajabol[j] += kajaArak[feladag] * resztvevok[i].ejSzam;
            fejenkent[j] += kajaArak[feladag] * resztvevok[i].ejSzam;
        }

        // szállás árának kiszámítása (sátor nélkül):
        if (resztvevok[i].szallTipus != "" && resztvevok[i]["eletkor"] >= 8) {
            // felnőttek (8 év felett):
            osszeg += szallasArak[holvan[resztvevok[i].szallTipus]].felnott * resztvevok[i].ejSzam;
            szallasbol[j] += szallasArak[holvan[resztvevok[i].szallTipus]].felnott * resztvevok[i].ejSzam;
            fejenkent[j] += szallasArak[holvan[resztvevok[i].szallTipus]].felnott * resztvevok[i].ejSzam;
        } else if (resztvevok[i].szallTipus != "" && 3 <= resztvevok[i]["eletkor"] && resztvevok[i]["eletkor"] < 8) {
            // gyerekek (3-8 év között):
            osszeg +=
              szallasArak[holvan[resztvevok[i].szallTipus]].gyerek *
              resztvevok[i].ejSzam;
            szallasbol[j] +=
              szallasArak[holvan[resztvevok[i].szallTipus]].gyerek *
              resztvevok[i].ejSzam;
            fejenkent[j] +=
              szallasArak[holvan[resztvevok[i].szallTipus]].gyerek *
              resztvevok[i].ejSzam;
        }

        // IFA számítása: 18 év felett 300 forint per éjszaka
        if (
          resztvevok[i]["eletkor"] >= 18 &&
          resztvevok[i]["eletkor"] <= 70 &&
          resztvevok[i]["ejSzam"] > 0 &&
          resztvevok[i]["szallTipus"] != "nem" &&
          resztvevok[i]["szallTipus"] != "sator"
        ) {
            osszeg += IFA * resztvevok[i]["ejSzam"];
            ifabol[j] += IFA * resztvevok[i]["ejSzam"];
            fejenkent[j] += IFA * resztvevok[i]["ejSzam"];
        }

    }
    // a felajánlott támogatást is hozzáadom a végösszeghez
    var tamogatas = parseInt(document.getElementById("tamogatasAdas").value);
    if (!isNaN(tamogatas)) {
        osszeg += tamogatas;
    }
    // sátorhely árának kiszámítása egyszerűsített (nem tökéletes) modellel:
    // összeszedem a sátrat kért résztvevők éjszakáinak számát
    // csökkenő sorrendbe rakom őket
    // és ahány sátorhelyet kértek, annyit veszek a lista elejéből.
    // elég béna és sánta, de a jelenlegi űrlappal nem látok jobb megoldást per pillanat.
    /*ezAJelentkezes.satorhelyekSzama = parseInt(document.getElementById("satorhelyekSzama").value); // a sátorhelyek száma onchange csak ezt a számolást hívja meg, így itt tárolom el az objektumba az értéket.
    if (isNaN(ezAJelentkezes.satorhelyekSzama)) ezAJelentkezes.satorhelyekSzama = 0;

    // akkor is össze kell számolnom a sátras résztvevőket, ha nincs kitöltve (mert kézzel törölte) a sátorhelyek számát a felhasználó.
    var satorejek = [];
    for (i = 0; i < resztvevok.length; i++) {
        if (resztvevok[i] === undefined) {
            continue;
        }
        if (resztvevok[i]["szallTipus"] == "sator") satorejek.push(resztvevok[i]["ejSzam"]);
    }
    // sorrendezek, hogy a hosszabb éjszám legyen a tömb elején:
    satorejek.sort(function(a, b) {
        return b - a;
    });

    // ellenőrzések, egyáltalán jók-e a bejövő paraméterek (sátorhelyszám vs sátras résztvevők száma)
    if (ezAJelentkezes.satorhelyekSzama > satorejek.length) {
        alert("Több sátorhelyet kértél, mint ahány résztvevőnél a szállás típusa sátor! Legfeljebb annyi sátorhely igényelhető, ahány sátras résztvevőd van!");
        return false;
    }
    if (satorejek.length>=1 && ezAJelentkezes.satorhelyekSzama==0) {
        alert("Vannak sátras résztvevők, de nem adtad meg, hogy hány sátorhelyet igényelsz! Állísd be a sátorhelyek számát!");
        return false;
    }

    // ahány sátorhelyet kérnek, annyiszor 1500*napokszáma,
    // amihez a napokszámát pedig csökkenő sorrendben a satorejek tömbből vesszük:
    for (let i = 0; i < ezAJelentkezes.satorhelyekSzama; i++) {
        if (!isNaN(satorejek[i])) {
            osszeg += satorejek[i] * 1500;
            satorbol += satorejek[i] * 1500;
        }
    }*/
 
    // eltárolom a végösszeget az osztályban és kitesztem a weboldalra is:
    ezAJelentkezes.vegosszeg = osszeg;
    /*document.getElementById("vegosszeg").innerText = osszeg + " Ft";
    document.getElementById("szallasbol").innerText = szallasbol + " Ft";
    document.getElementById("kajabol").innerText = kajabol + " Ft";
    document.getElementById("tamogatasbol").innerText = tamogatasbol + " Ft";*/
    document.getElementById("arnev").innerHTML = "<th>Név:</th>";
    document.getElementById("arszallas").innerHTML = "<th>Szállás:</th>";
    document.getElementById("arifa").innerHTML = "<th>IFA:</th>";
    document.getElementById("arkaja").innerHTML = "<th>Étkezés:</th>";
    document.getElementById("arfejenkent").innerHTML = "<th>Összesen:</th>";
    for (let i = 0; i < nevek.length; i++) {
        let nevHtml = document.createElement("td");
        nevHtml.innerText = nevek[i];
        document.getElementById("arnev").appendChild(nevHtml);

        let szallasbolHtml = document.createElement("td");
        szallasbolHtml.innerText = tagolas(szallasbol[i]) + " Ft";
        document.getElementById("arszallas").appendChild(szallasbolHtml);

        let ifabolHtml = document.createElement("td");
        ifabolHtml.innerText = tagolas(ifabol[i]) + " Ft";
        document.getElementById("arifa").appendChild(ifabolHtml);
        
        let kajabolHtml = document.createElement("td");
        kajabolHtml.innerText = tagolas(kajabol[i]) + " Ft";
        document.getElementById("arkaja").appendChild(kajabolHtml);
        
        let fejenkentHtml = document.createElement("td");
        fejenkentHtml.innerText = tagolas(fejenkent[i]) + " Ft";
        document.getElementById("arfejenkent").appendChild(fejenkentHtml);
    }

    document.getElementById("vegosszeg").innerText = tagolas(osszeg) + " Ft";
    //document.getElementById("satorara").innerText = satorbol + " Ft";


    // végigfutott a számítás hibátlanul, beküldhetőre állítom:
    ezAJelentkezes.bekuldheto = true;
    return true;
}

function registerSubmit() {

    event.preventDefault();

    
    // először a jelentkezes kötelező mezőit ellenőrizzük:
    
    //kapcsolattartó mezők kitöltése kötelező:
    if (ezAJelentkezes["kapcsNev"] == "" || ezAJelentkezes["kapcsMobil"] == "" || ezAJelentkezes["kapcsEmail"] == "") {
        alert("A kapcsolattartó mindhárom mezőjét pontosan ki kell tölteni!");
        return false;
    }

    // másodszor a minden resztvevo kötelező mezőinek értékét ellenőrzöm:
    
    var mehet = false;
    for (let i = 0;i<resztvevok.length;i++) {
        
        // a név nélküli sorokat egyszerűen törlöm.
        // ez jellemzően az utolsó sor lehet, amelyet véletlen fűztek a lista végére és nem töröltek ki.
        if (resztvevok[i] !== undefined) {
            if (resztvevok[i].nev=="") {
                sorTorles(document.getElementById("kuka"+resztvevok[i].HTMLid));
            }
            else {
                // kötelező mezőknek van értéke?
                if (resztvevok[i]["nev"] != ""
                    && resztvevok[i]["szulido"]!=""
                    && resztvevok[i]["szulhely"]!=""
                    && resztvevok[i]["lakcim"]!=""
                    && resztvevok[i]["eletkor"] != 0
                    && resztvevok[i]["szallTipus"] != ""
                    && resztvevok[i]["ejSzam"] != null
                    /*&& resztvevok[i]["kaja"]["szerda"].length > 0
                    && resztvevok[i]["kaja"]["csutortok"].length > 0
                    && resztvevok[i]["kaja"]["pentek"].length > 0
                    && resztvevok[i]["kaja"]["szombat"].length > 0
                    && resztvevok[i]["kaja"]["vasarnap"].length > 0*/
                    /*
                    && resztvevok[i]["csutortokEbed"] != ""
                    && resztvevok[i]["csutortokVacs"] != ""
                    && resztvevok[i]["pentekReg"] != ""
                    && resztvevok[i]["pentekEbed"] != ""
                    && resztvevok[i]["pentekVacs"] != ""
                    && resztvevok[i]["szombatReg"] != ""
                    && resztvevok[i]["szombatEbed"] != ""
                    && resztvevok[i]["szombatVacs"] != ""
                    && resztvevok[i]["vasarnapReg"] != ""
                    && resztvevok[i]["vasarnapEbed"] != ""
                    */) {
                    mehet = true;
                } else {
                    mehet = false;
                    break;
                }
            }
        }
    }

    if (!mehet) {
        alert("Nincs az összes kötelező mező kitöltve a résztvevőknél!");
        return false;
    }

    // gyanús, ha a végösszeg még mindig 0
    if (ezAJelentkezes.vegosszeg==0 || isNaN(ezAJelentkezes.vegosszeg)) {
        alert("A fizetendő végösszeg nulla, valószínűleg nincs az összes kötelező mező kitöltve!");
        return false;            
    }
    
    // vagy bármi más ok miatt nem beküldhető (egyelőre a költségszámításnál ez csak a sátorhely számánál használom)
    if (ezAJelentkezes["bekuldheto"]===false) {
        alert("A sátorhelyek száma és a sátras résztvevők száma nem stimmel, javítsd kérlek!");
        return false;            
    }

    // az adatkezelési tájékoztatót el kell fogadni:

    if (!document.getElementById("adatkezelesi").checked || !document.getElementById("adatkezelesi2").checked) {
        alert("Az adatkezelés tájékoztatót fogadd el a jelölőnégyzetek kipipálásával!");
        return false;
    }

    var bekuldesConfirm = confirm('Elküldhetem a jelentkezést a szervezőknek?');
    if (bekuldesConfirm == true && mehet) {

        regisztracioElkuldese();
        alert("Elküldtem a jelentkezést!");

    } else {
        alert("Nem küldtem el a jelentkezést!");
    }
}

function regisztracioElkuldese() {

    //TODO: jó AJAX hibakezelést és visszajelzést megvalósítani!

    var mehet = true;
    var xhttp = new XMLHttpRequest();
    var fd = new FormData(document.getElementById("registerForm"));
    fd.append("resztvevoIndex", resztvevoIndex);
    fd.append("resztvevokSzama", resztvevokSzama);
    fd.append("vegosszeg",ezAJelentkezes.vegosszeg);
    //fd.append("satorhelyekSzama",ezAJelentkezes.satorhelyekSzama);
    fd.append("adatkezeles_hozzajarul", ezAJelentkezes.adatkezeles_hozzajarul);
    for (i = 0;i<resztvevok.length;i++) {
        if (resztvevok[i] !== undefined && mehet == true) {
            fd.append("htmlIds[]",i);
            //if (!fd.has("altabor"+i)) fd.append("altabor"+i,resztvevok[i].altabor);

            // az ételeknél az egyes mezőkbe az kerül, hogy "nem"/"fel"/"egesz"
            // mert eredetileg azt hittük, étkezésenként is lehet majd egesz/fel között változtatni,
            // de végül egyszerűsítve lett, hogy az egész és fél résztvevőnként választható ki,
            // de az adatbázis már így maradt.
            //let adag = resztvevok[i]["feladag"]; // az érték "egesz" vagy "fel" és nem lehet más

            //beállítom a default-nak a nemet minden mezőre:
            /*let kajatomb = {
                szerdaVacs: "nem",
                csutortokReg: "nem",
                csutortokEbed: "nem",
                csutortokVacs: "nem",
                pentekReg: "nem",
                pentekEbed: "nem",
                pentekVacs: "nem",
                szombatReg: "nem",
                szombatEbed: "nem",
                szombatVacs: "nem",
                vasarnapReg: "nem",
                vasarnapEbed: "nem",
                vasarnapVacs: "nem"
            };*/

            // a tényleges kaja igényt beírom a fenti változókba, ha van:
            /*for (let nap in resztvevok[i]["kaja"]) {
                for (let etkezes of resztvevok[i]["kaja"][nap]) {
                    if (etkezes != "semmi") {
                        etkezes = etkezes[0].toUpperCase()+etkezes.slice(1);
                        kajatomb[nap + etkezes] = adag;
                    }
                }
            }*/

            // beteszem a kész kajatomb-öt a FormData-ba úgy, hogy a nap-étkezés-hez a jelentkező sorszámát is hozzáfűzöm:
            /*for (let nap in kajatomb) {
                fd.append(nap+i,kajatomb[nap]);
            }*/

            /*
            fd.append("szerdaVacs"+i,resztvevok[i].szerdaVacs);
            fd.append("csutortokReg"+i,resztvevok[i].csutortokReg);
            fd.append("csutortokEbed"+i,resztvevok[i].csutortokEbed);
            fd.append("csutortokVacs"+i,resztvevok[i].csutortokVacs);
            fd.append("pentekReg"+i,resztvevok[i].pentekReg);
            fd.append("pentekEbed"+i,resztvevok[i].pentekEbed);
            fd.append("pentekVacs"+i,resztvevok[i].pentekVacs);
            fd.append("szombatReg"+i,resztvevok[i].szombatReg);
            fd.append("szombatEbed"+i,resztvevok[i].szombatEbed);
            fd.append("szombatVacs"+i,resztvevok[i].szombatVacs);
            fd.append("vasarnapReg"+i,resztvevok[i].vasarnapReg);
            fd.append("vasarnapEbed"+i,resztvevok[i].vasarnapEbed);
            */
        }
    }

    xhttp.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            document.getElementById("valaszhelye").innerHTML = this.responseText;
            window.scrollTo(0,0);
            document.getElementById("urlaphelye").style.display = "none";
        }
    }
    if (mehet == true) {
        xhttp.open("POST", "regisztracio.php");
        xhttp.send(fd);
    }
}

function sorTorles(e) {
    var kukaSzam = e.id.slice(4);
    delete resztvevok[kukaSzam];
    e.parentNode.parentNode.remove();
    resztvevokSzama--;

    // sor törlése után a számítást újra el kell végezni, mivel a törölt sornak lehetett már költsége:
    koltsegSzamitas();
}

function bodyBetoltes() {
    sablonBetoltes('regisztraciosUrlap.html', 1);
    // a verziót kiteszem a lap aljára, hogy probléma esetén látható legyen, megfelelő app.js volt-e betöltve.
    document.getElementById("debug").innerHTML="<p style=\"font-size:8px\">Alkalmazás verzió: "+verzio+"</p>";
}

function datumInputOnchange(e) {
    eletkorSzamitas(e);
    betoltesObjektumba(e);
}

function lakcimOnchange(e) {
    // mindig az utoljára megadott lakcímmel megyek tovább, mint családi lakcím,
    // ezt akkor használom, amikor új résztvevőt adok hozzá.
    ezAJelentkezes.csaladLakcime = e.value;
    betoltesObjektumba(e);
}

function betoltesObjektumba(e) {
    var resztvevoId = e.parentNode.parentNode.id.slice(9);
    var mezoNev = e.name.substring(0, e.name.length-resztvevoId.toString().length);
    /*if (resztvevok[resztvevoId]["feladag"]) {
        var milyenAdag = "fel";
    } else {
        var milyenAdag = "egesz";
    }*/
    switch (mezoNev) {
        /*case "szerda":
        case "csutortok":
        case "pentek":
        case "szombat":
        case "vasarnap":
            let kertKaja = e.value.split(',');
            //if (!resztvevok[resztvevoId]['kaja']) resztvevok[resztvevoId]['kaja'] = {};
            resztvevok[resztvevoId]['kaja'][mezoNev] = kertKaja;
            break;*/
        
        case "ejSzam":
            resztvevok[resztvevoId].ejSzamNappal = e.value;
            resztvevok[resztvevoId].ejSzam = parseInt(e.value[0]);
            break;

        /*case "feladag":
            // féladagot csak 7 éves kor alatt lehet kérni:
            if (e.value == "fel") {
                if (resztvevok[resztvevoId].eletkor>6) {
                    alert("Fél adag ételt csak 6 éves korig lehet választani.");
                    e.value="egesz";
                }
            }
            resztvevok[resztvevoId][mezoNev] = e.value;
            break;*/

        default:
            resztvevok[resztvevoId][mezoNev] = e.value;
            break;
    }

    // ha szállás típusának sátrat állít be a felhasználó és a sátorhelyek száma üres,
    // akkor a sikeres kalkuláció érdekében a sátrohelyek számát 1-re állítom.
    /*if (mezoNev == "szallTipus" && e.value == "sator") {
        let sSz = document.getElementById("satorhelyekSzama");
        if (isNaN(parseInt(sSz.value))) {
            sSz.value = 1;
            ezAJelentkezes.satorhelyekSzama = 1;
        }
    }*/
    koltsegSzamitas();
}

function betoltesJelentkezesObjektumba(e) {
    if (e.id == "adatkezeles_hozzajarul") {
        ezAJelentkezes[e.id] = e.checked ? "igen" : "nem";
    } else {
        ezAJelentkezes[e.id] = e.value;
    }
}
