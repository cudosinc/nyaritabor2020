<!DOCTYPE html>
<html>
<head>
    <title>Nyári tábor jelentkezése teljes listája</title>
    <meta charset="utf-8">
    <style>
        table,th,td {
            border: 1px solid black;
            text-align: left;
        }
    </style>
</head>
<body>
<?php
// az adatbázis tartalmát egyszerű táblázatban megjeleníti.

require("../parameterek.php");

echo("<h1>Nyári tábor jelentkezések teljes listája</h1>");

$mysqli = new mysqli($db_server, $db_user, $db_pass, $db_name);

// Oh no! A connect_errno exists so the connection attempt failed!
if ($mysqli->connect_errno) {

    echo "Sorry, this website is experiencing problems.";

    // Something you should not do on a public site, but this example will show you
    // anyways, is print out MySQL error related information -- you might log this
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    
    // You might want to show them something nice, but we will simply exit
    exit;
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
    exit();
}

$sql = "SELECT jelentkezes.*,resztvevo.* FROM jelentkezes,resztvevo  WHERE jelentkezes.id=resztvevo.jelentkezes_id;";

if (!$result = $mysqli->query($sql)) {
    // Oh no! The query failed. 
    echo "Az adatbázis lekédezése nem sikerült!";

    // Again, do not do this on a public site, but we'll show you how
    // to get the error information
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

// tábla fejléce

echo("<table><thead>\r\n<tr>");
echo("<th>sorszám</th>");
echo("<th>kapcs.név</th>");
echo("<th>kapcs.email</th>");
echo("<th>kapcs.mobil</th>");
echo("<th>jelentkezés dátuma</th>");
//echo("<th>mód.dátum</th>");
echo("<th>tám.adás</th>");
echo("<th>tám.kérés</th>");
echo("<th>végösszeg</th>");
echo("<th>sátorhelyek száma</th>");
echo("<th>hálótársak</th>");
echo("<th>busz</th>");
echo("<th>vállalás</th>");
echo("<th>üzenet</th>");
//echo("<th>URL</th>");
//echo("<th>résztvevő-ID</th>");
echo("<th>név</th>");
echo("<th>születési hely</th>");
echo("<th>születési idő</th>");
echo("<th>lakcím</th>");
echo("<th>gyerek altábor</th>");
echo("<th>szállás típusa</th>");
echo("<th>éjszakák száma</th>");
echo("<th>csüt. ebéd</th>");
echo("<th>csüt. vacs</th>");
echo("<th>pént. reggeli/th>");
echo("<th>pént. ebéd</th>");
echo("<th>pént. vacsora</th>");
echo("<th>szomb. reggeli</th>");
echo("<th>szomb. ebéd</th>");
echo("<th>szomb. vacsora</th>");
echo("<th>vas. reggeli</th>");
echo("<th>vas. ebéd</th>");
echo("<th>allergia</th>");
echo("</tr></thead>\r\n");

// tábla törzse

echo("<tbody>");
while ($row = $result->fetch_assoc()) {
    echo("<tr>");
    echo("<td>".$row['jelentkezes_id']."</td>");
    echo("<td>".$row['kapcsNev']."</td>");
    echo("<td>".$row['kapcsEmail']."</td>");
    echo("<td>".$row['kapcsMobil']."</td>");
    echo("<td>".$row['jelentkezesDatum']."</td>");
  //  echo("<td>".$row['modDatum']."</td>");
    echo("<td>".$row['tamogatasAdas']."</td>");
    echo("<td>".$row['tamogatasKeres']."</td>");
    echo("<td>".$row['vegosszeg']."</td>");
    echo("<td>".$row['satorhelyekSzama']."</td>");
    echo("<td>".$row['halotarsak']."</td>");
    echo("<td>".$row['busz']."</td>");
    echo("<td>".$row['feladat']."</td>");
    echo("<td>".$row['uzenet']."</td>");
    //echo("<td>".$row['URL']."</td>");
    echo("<td>".$row['nev']."</td>");
    echo("<td>".$row['szulhely']."</td>");
    echo("<td>".$row['szulido']."</td>");
    echo("<td>".$row['lakcim']."</td>");
    echo("<td>".$row['altabor']."</td>");
    echo("<td>".$row['szallTipus']."</td>");
    echo("<td>".$row['ejSzam']."</td>");
    echo("<td>".$row['csutortokEbed']."</td>");
    echo("<td>".$row['csutortokVacs']."</td>");
    echo("<td>".$row['pentekReg']."</td>");
    echo("<td>".$row['pentekEbed']."</td>");
    echo("<td>".$row['pentekVacs']."</td>");
    echo("<td>".$row['szombatReg']."</td>");
    echo("<td>".$row['szombatEbed']."</td>");
    echo("<td>".$row['szombatVacs']."</td>");
    echo("<td>".$row['vasarnapReg']."</td>");
    echo("<td>".$row['vasarnapEbed']."</td>");
    echo("<td>".$row['allergia']."</td>");
    echo("</tr>\r\n");
}

/* close connection */
$mysqli->close();
?>
</tbody>
</table>
</body>
</html>
