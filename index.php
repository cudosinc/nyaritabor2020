<!DOCTYPE html>
<html>
<head>
    <title>Nyári tábor regisztráció - 2023. július 27-30.</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <script src="app.js"></script>
</head>
<body onload="bodyBetoltes();" id="body">
    <?php include 'hely.php'; ?>
    <div id="valaszhelye"></div>
    <div id="urlaphelye"></div>
    <div id="debug"></div>
</body>
</html>